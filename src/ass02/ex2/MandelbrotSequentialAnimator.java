package ass02.ex2;

/**
 * Simple Mandelbrot Set Viewer
 *
 * @author aricci
 */
public class MandelbrotSequentialAnimator {

    public static final int    WIDTH  = 400;
    public static final int    HEIGHT = 400;
    public static final int    N_ITER = 500;
    public static       double radius = 2;


    public static void main(String[] args) throws Exception {

        Complex c0 = new Complex(-0.75, 0);
        Complex c1 = new Complex(-0.75, 0.1);
        Complex c2 = new Complex(-0.1011, 0.9563);
        Complex c3 = new Complex(0.254, 0);
        Complex c4 = new Complex(0.001643721971153, 0.822467633298876);

		/* creating the set */
        MandelbrotSetImage manager = new MandelbrotExecutorsManager(c4, radius);

        System.out.println("Computing w:" + WIDTH + "|h:" + HEIGHT + "|nIt:" + N_ITER + "...");

		/* showing the image */
        MandelbrotView view = new MandelbrotView(manager);
        view.setVisible(true);
    }
}
