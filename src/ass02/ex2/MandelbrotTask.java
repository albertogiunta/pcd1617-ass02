package ass02.ex2;

import java.util.concurrent.Callable;

import static ass02.ex2.MandelbrotSequentialAnimator.HEIGHT;
import static ass02.ex2.MandelbrotSequentialAnimator.WIDTH;

@SuppressWarnings("Duplicates")
public class MandelbrotTask implements Callable<Boolean> {

    private int nIterMax, row;
    private int     image[];
    private Complex center;
    private double  delta;

    public MandelbrotTask(Complex c, double radius, int nIterMax, int row, int[] image) {
        this.row = row;
        this.image = image;
        this.center = c;
        this.nIterMax = nIterMax;
        this.delta = radius;
    }

    @Override
    public synchronized Boolean call() throws Exception {
        for (int x = 0; x < WIDTH; x++) {
            double x0    = (x - WIDTH * 0.5) * delta + center.re();
            double y0    = center.im() - (row - HEIGHT * 0.5) * delta;
            double level = computeColor(x0, y0, nIterMax);
            int    color = (int) (level * 255);
            image[row * WIDTH + x] = color + (color << 8) + (color << 16);
        }
        return true;
    }

    private synchronized double computeColor(double x0, double y0, int maxIteration) {
        int    iteration = 0;
        double x         = 0;
        double y         = 0;
        double x2        = x * x;
        double y2        = y * y;
        while (x2 + y2 < 4 && iteration < maxIteration) {
            double xtemp = x2 - y2 + x0;
            y = 2 * x * y + y0;
            x = xtemp;
            x2 = x * x;
            y2 = y * y;
            iteration++;
        }

        if (iteration == maxIteration) {
            return 0;
        } else {
            return 1.0 - ((double) iteration) / maxIteration;
        }
    }
}
