package ass02.ex2;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import static ass02.ex2.MandelbrotSequentialAnimator.HEIGHT;
import static ass02.ex2.MandelbrotSequentialAnimator.WIDTH;

public class MandelbrotExecutorsManager implements MandelbrotSetImage {

    private int     image[];
    private Complex center;
    private double  delta;

    public MandelbrotExecutorsManager(Complex center, double radius) {
        this.image = new int[WIDTH * HEIGHT];
        this.center = center;
        this.delta = radius / (WIDTH * 0.5);
    }


    @Override
    public void compute(int nIterMax) {
        ExecutorService       executorService = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors() + 1);
        List<Future<Boolean>> futures         = new ArrayList<>();
        for (int row = 0; row < HEIGHT; row++) {
            futures.add(executorService.submit(new MandelbrotTask(center, delta, nIterMax, row, image)));
        }

        for (Future<Boolean> future : futures) {
            try {
                future.get();
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }
        executorService.shutdown();
        try {
            executorService.awaitTermination(Long.MAX_VALUE, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Complex getPoint(int x, int y) {
        return new Complex((x - WIDTH * 0.5) * delta + center.re(), center.im() - (y - HEIGHT * 0.5) * delta);
    }

    @Override
    public int getHeight() {
        return HEIGHT;
    }

    @Override
    public int getWidth() {
        return WIDTH;
    }

    @Override
    public int[] getImage() {
        return this.image;
    }

    @Override
    public void updateRadius(double rad) {
        delta = rad / (WIDTH * 0.5);
    }
}
