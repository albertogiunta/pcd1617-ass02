package ass02.ex1;

import java.util.Random;

import static ass02.ex1.Main.MAX_VALUE;

public class Player implements Runnable {

    private int playerId;
    private long value = new Random().nextInt(MAX_VALUE);
    private long tries = 0;
    private Oracle oracle;

    public Player(int playerId, Oracle oracle) {
        this.playerId = playerId;
        this.oracle = oracle;
        System.out.println("[PLAYER " + playerId + "] Started with guess " + value);
    }

    @Override
    public void run() {
        String finalString = "sob...";
        while (!this.oracle.isGameFinished()) {
            try {
                Result result = oracle.tryToGuess(this.playerId, value);
                tries++;
                if (result.isGreater()) {
                    System.out.println("[PLAYER " + playerId + "] Failed");
                    value++;
                } else if (result.isLess()) {
                    System.out.println("[PLAYER " + playerId + "] Failed");
                    value--;
                } else if (result.found()) {
                    finalString = "FOUND THE NUMBER with " + tries + " iterations";
                    break;
                }
            } catch (GameFinishedException ignored){}
        }
        System.out.println("[PLAYER " + playerId + "] " + finalString);
    }
}
