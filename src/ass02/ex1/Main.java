package ass02.ex1;

import java.util.LinkedList;
import java.util.List;

public class Main {

    public static final int N_PLAYERS = 40;
    public static final int MAX_VALUE = 500;

    public static void main(String[] args) {

        Oracle       oracle  = new Oracle();
        List<Thread> players = new LinkedList<>();

        for (int i = 0; i < N_PLAYERS; i++) {
            players.add(new Thread(new Player(i, oracle)));
        }

        for (Thread p : players) {
            p.start();
        }

        for (Thread p : players){
            try {
                p.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
