package ass02.ex1;

public class ResultImpl implements Result {

    private long secret;
    private long guess;

    public ResultImpl(long secret, long guess) {
        this.secret = secret;
        this.guess = guess;
    }

    @Override
    public boolean found() {
        return secret == guess;
    }

    @Override
    public boolean isGreater() {
        return secret > guess;
    }

    @Override
    public boolean isLess() {
        return secret < guess;
    }
}
