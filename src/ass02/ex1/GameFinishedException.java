package ass02.ex1;

public class GameFinishedException extends Exception {

    private int winnerId;

    public GameFinishedException(int winnerId) {
        super("");
        this.winnerId = winnerId;
    }

    public int getWinnerId() {
        return winnerId;
    }
}
