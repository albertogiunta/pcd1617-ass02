package ass02.ex1;

import java.util.HashSet;
import java.util.Random;

import static ass02.ex1.Main.MAX_VALUE;
import static ass02.ex1.Main.N_PLAYERS;

public class Oracle implements OracleInterface {

    private int secret = new Random().nextInt(MAX_VALUE);
    private int              winner  = -1;
    private boolean          isFound = false;
    private HashSet<Integer> players = new HashSet<>();

    public Oracle() {
        System.out.println("[ORACLE] Created with secret " + secret);
    }

    @Override
    public synchronized boolean isGameFinished() {
        return isFound;
    }

    @Override
    public synchronized Result tryToGuess(int playerId, long value) throws GameFinishedException {

        while (!isGameFinished()) {
            // if element already present
            if (!players.add(playerId)) {
                try {
                    wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } else {
                System.out.println("[ORACLE] Playing with player " + playerId);
                break;
            }
        }

        if (isGameFinished()) {
            throw new GameFinishedException(winner);
        }

        Result result = new ResultImpl(secret, value);
        isFound = result.found();
        if (isGameFinished()) {
            winner = playerId;
            notifyAll();
        }

        if (players.size() == N_PLAYERS) {
            players.clear();
            notifyAll();
            System.out.println("\n---------- finished turn, freeing everyone ----------\n");
        }

        System.out.println("[ORACLE] Players who tried to guess are " + players.toString());
        return result;
    }
}